if (Drupal.jsEnabled) {
  var cookie = document.cookie.split(";");
  for (var i=0;i<cookie.length;i++) {
    if (cookie[i].substr(0,18).match("comment_info_name")) {
      document.cookie = 'comment_info_name=;expires=Thu, 01-Jan-1970 00:00:01 GMT;domain=;path=/';
    }
    if (cookie[i].substr(0,22).match("comment_info_homepage")) {
      document.cookie = 'comment_info_homepage=;expires=Thu, 01-Jan-1970 00:00:01 GMT;domain=;path=/';
    }
    if (cookie[i].substr(0,18).match("comment_info_mail")) {
      document.cookie = 'comment_info_mail=;expires=Thu, 01-Jan-1970 00:00:01 GMT;domain=;path=/';
    }
  }
}
